import json

from app import app
from flask import Flask, request, Response

@app.route('/')
def index():
    print('index')
    return render_template('index.html')


@app.route('/webhook', methods=['POST'])
def respond():
    #print(request.json);
    print(json.dumps(request.json, indent=4, sort_keys=True))
    return Response(status=200)
