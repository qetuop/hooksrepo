# flaskSkeleton


python3 -m venv venv
source venv/bin/activate
#pip install flask
#pip install flask_restful
#pip install flask_sqlalchemy
pip freeze | grep -v "pkg-resources" > requirements.txt


# REPONAME
REPODESC

### Setup
Ensure all system packages are installed for the version of python you are using ex:
```
sudo apt-get install python3.8
sudo apt-get install python3.8-venv
sudo apt-get install python3.8-dev
```

### Build
```
git clone https://github.com/qetuop/REPONAME.git
cd REPONAME
python3.8 -m venv venv  
source venv/bin/activate
pip install -r requirements.txt

```

### Run
```python3.8 poetory.py``` 
